package consensusLayer

import org.junit.jupiter.api.Test
import org.assertj.core.api.Assertions.*

class StringUtilTest {

    private val target256: String = "3c57dabc9514286a9201409f0600ecd94b486636d330a716e7e995b178f509c5".toUpperCase()

    @Test
    fun applySHA256() {
        assertThat(StringUtil.applySHA256("My name is Carlos.")).isEqualTo(target256)
    }

    private val target512: String = StringUtil.applySHA512("My name is Carlos.")

    @Test
    fun applySHA512() {
        assertThat(StringUtil.applySHA512("My name is Carlos.")).isEqualTo(target512)
    }
}