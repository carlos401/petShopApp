package consensusLayer

import org.junit.jupiter.api.*
// This library was requested from:
// http://joel-costigliola.github.io/assertj/assertj-core-quick-start.html
import org.assertj.core.api.Assertions.*

// Important info about unit testing, here: https://phauer.com/2018/best-practices-unit-testing-kotlin/

// This way, a single instance of the test class is used for every method
// Only available with JUnit5
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class BlockTest {

    private val blockTest: Block = Block(
        "My name is Carlos.",
        "0"
    )

    @Nested
    inner class BlockData{
        @Test
        fun `getData`() {
            assertThat(blockTest.data).isEqualTo("My name is Carlos.")
        }


    }

    @Nested
    inner class TimeStamping{
        @Test
        fun `getTimeStamp`() {
            assertThat(blockTest.timeStamp).isNotEmpty()
        }
    }
}