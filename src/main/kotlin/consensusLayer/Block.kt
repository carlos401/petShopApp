// packaging directive
package consensusLayer

import java.time.LocalDateTime

/**
 * This class is the minimal structure for a blockchain
 * @author Carlos Delgado Rojas
 * @version 1.0
 *
 * @param data data to be saved in the block
 * @param previousHash hash of the previous block in the chain
 */
class Block constructor(val data: String,
                        val previousHash: String) {

    // the calculated hash for this block
    val hash: String
    // the timestamp assigned to this block
    val timeStamp: String = LocalDateTime.now().toString()

    // the initialization code
    init {
        this.hash = StringUtil.applySHA256(
            previousHash + timeStamp + data
        )
    }
}