package consensusLayer
import java.security.MessageDigest

/**
 * This class provides functions for string manipulation
 * @author Carlos Delgado Rojas
 * @version 1.0
 *
 */
object StringUtil {

    /**
     * Applies Sha256 to a string and returns the result
     * @param input the string to be modified
     * @return the String with SHA256 applied
     */
    fun applySHA256(input: String): String{
        return this.hashString("SHA-256",input)
    }

    /**
     * Applies Sha512 to a string and returns the result
     * @param input the string to be modified
     * @return the String with SHA512 applied
     */
    fun applySHA512(input: String): String{
        return this.hashString("SHA-512", input)
    }

    /**
     * Hashing Utils
     * @author Sam Clarke
     * @license MIT
     *
     * Accessed from https://www.samclarke.com/kotlin-hash-strings/
     */
    private fun hashString(type: String, input: String):String{
        val HEX_CHARS = "0123456789ABCDEF"
        val bytes = MessageDigest
            .getInstance(type)
            .digest(input.toByteArray())
        val result = StringBuilder(bytes.size * 2)

        bytes.forEach {
            val i = it.toInt()
            result.append(HEX_CHARS[i shr 4 and 0x0f])
            result.append(HEX_CHARS[i and 0x0f])
        }
        return result.toString()
    }
}